/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  NativeEventEmitter,
  NativeModules,
  FlatList,
  RefreshControl,
  ScrollView
} from 'react-native';
import RNSparkonAdminLib from 'react-native-sparkon-admin-lib';
import { List, ListItem, SearchBar } from "react-native-elements";
import {TimerMixin} from 'react-timer-mixin';
import reactMixin from 'react-mixin';


var buttonState = "init";

export default class bleTester extends Component {
  constructor(props) {
      super(props);

      this.state = {
        loading: false,
        data: [{name:'mac'}],
        page: 1,
        seed: 1,
        error: null,
        refreshing: false,
        myText: 'My Original Text'
      };

                
    }

    makeRemoteRequest = () => {
    
      setTimeout(() => {
          RNSparkonAdminLib.availableSensors().then((peripData) => {
           
            const { page, seed } = this.state;
            this.setState({
            data: peripData,
            refreshing: false
          })

        })

      }, 200);

  };
          

      
      

handleRefresh = () => {
  this.setState(
    {
      page: 1,
      seed: this.state.seed + 1,
      refreshing: true,
      myText: 'ok'
    },
    () => {
      this.makeRemoteRequest();
    }
  );
};
updateText(text) {
  this.setState({myText: text})
};
_bleInit() {
  RNSparkonAdminLib.initBle("10203");
};
_bleInit() {
  RNSparkonAdminLib.initBle("10203");
};


onClick = (name) => {
  if(buttonState.localeCompare("init") == 0){
    RNSparkonAdminLib.adminConnectTo(name, "3031323334353637");
    buttonState = "nonlog";
  }else if(buttonState.localeCompare("nonlog") == 0){
    buttonState = "getLog";
    RNSparkonAdminLib.sensorLogOn(name);
  }else if(buttonState.localeCompare("getLog") == 0){
    buttonState = "log";
    RNSparkonAdminLib.getSensorLog(name);
  }else if(buttonState.localeCompare("log") == 0){
    buttonState = "nonlog";
    RNSparkonAdminLib.sensorLogOff(name);
  }
};
render() {
  
  return (
    <View>
      <ScrollView
        style={{height:200}}>
      <Text style={styles.instructions}>
        Edited text
      </Text>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._bleInit()}}
        title="BLuetooth init!">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._logAdminOn()}}
        title="Log Admin On">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._logAdminOff()}}
        title="Log Admin Off">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._sensorReset()}}
        title="Reset">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._blePowerOff()}}
        title="blePowerOff">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._bootloaderUpdateReq()}}
        title="bootloaderUpdateReq">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._bootloaderUpload()}}
        title="bootloaderUpload">
      </Button>
      <Button
        style={{borderWidth: 1, borderColor: 'blue'}}
        onPress={()=>{this._bleSend()}}
        title={this.state.myText}>
      </Button>
      </ScrollView>
      <List containerStyle={{ borderTopWidth:3, borderBottomWidth: 3, height: 300 , width: 300}}
        style={{height:200}}>
      <FlatList
      refreshControl={
        <RefreshControl
        onRefresh={()=>{this.handleRefresh()}}
        refreshing={this.state.refreshing}
        />
      }
        data={this.state.data}
        renderItem={({ item }) => (
          <ListItem button
          style={{height:50}}
            title={`${item["name"]}`}
            onPress={()=>{this.onClick(item["name"])}}
        />
        )}
      />
    </List>
    </View>
  );
}
}

  

    

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    height:2000,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('bleTester', () => bleTester);
